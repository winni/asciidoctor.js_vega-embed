import {
	convertAdocFile,
	getAdocFilenameFromArgv,
	normalizeSeparator,
} from "./helper.js";
import browserSync from "browser-sync";
import chokidar from "chokidar";

const adocFilename = getAdocFilenameFromArgv();
const htmlFilename = adocFilename.replace(".adoc", ".html");
const bs = browserSync.create();

chokidar
	.watch(".", {
		ignored: [
			/(^|[\/\\])\../, // ignore dot files (https://github.com/paulmillr/chokidar)
			"node_modules",
		],
	})
	.on("change", watchCallback);

bs.init({
	browser: "chrome",
	server: true,
	startPath: htmlFilename,
});

convertAdocFileAndLog("startup");

/**
 * @param {string} cause
 */
function convertAdocFileAndLog(cause) {
	convertAdocFile(adocFilename);
	console.log(`Converted file: '${adocFilename}' (${cause})`);
}

/**
 * @param {string} filename
 */
function watchCallback(filename) {
	const normalizedFilename = normalizeSeparator(filename);
	if (normalizedFilename == htmlFilename) return;

	convertAdocFileAndLog(`'${normalizedFilename}' changed`);
	bs.reload();
}
