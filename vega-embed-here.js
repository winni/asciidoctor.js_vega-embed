/**
 * @typedef {import("vega-embed/src/embed").EmbedOptions} EmbedOptions
 * @typedef {import("vega-embed/src/embed").VisualizationSpec} VisualizationSpec
 * @typedef {import("vega-embed/src/embed").Result} Result
 * @typedef {(el: null | SVGScriptElement | HTMLElement | string, spec: VisualizationSpec | string, opts?: EmbedOptions) => Promise<Result>} VegaEmbed
 */

/** @type EmbedOptions */
const embedOptions = {
	actions: false,
	config: {
		autosize: {
			contains: "padding",
			type: "fit",
		},
		// double default font sizes
		fontSize: {
			groupSubtitle: 24,
			groupTitle: 26,
			guideLabel: 20,
			guideTitle: 22,
			text: 22,
		},
		line: {
			size: 4,
		},
		mark: {
			tooltip: true,
		},
		point: {
			size: 80,
		},
		text: {
			color: "white",
		},
		view: {
			continuousHeight: 300,
			continuousWidth: 1000,
			discreteHeight: 300,
			discreteWidth: 500,
		},
	},
	formatLocale: {
		currency: ["", "\u00a0€"],
		decimal: ",",
		grouping: [3],
		thousands: ".",
	},
	renderer: "svg",
	theme: "dark",
	timeFormatLocale: {
		date: "%d.%m.%Y",
		dateTime: "%A, der %e. %B %Y, %X",
		days: [
			"Sonntag",
			"Montag",
			"Dienstag",
			"Mittwoch",
			"Donnerstag",
			"Freitag",
			"Samstag",
		],
		months: [
			"Januar",
			"Februar",
			"März",
			"April",
			"Mai",
			"Juni",
			"Juli",
			"August",
			"September",
			"Oktober",
			"November",
			"Dezember",
		],
		periods: ["AM", "PM"],
		shortDays: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
		shortMonths: [
			"Jan",
			"Feb",
			"Mrz",
			"Apr",
			"Mai",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Okt",
			"Nov",
			"Dez",
		],
		time: "%H:%M:%S",
	},
};

/** @type VegaEmbed */
var vegaEmbed;

/**
 * @param {string} filename
 */
function vegaEmbedHere(filename) {
	return vegaEmbed(document.currentScript, filename, embedOptions);
}
