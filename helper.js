import asciidoctor from "@asciidoctor/core";
import asciidoctorRevealjs from "@asciidoctor/reveal.js";
import assert from "assert";

const processor = asciidoctor();
asciidoctorRevealjs.register();

export function getAdocFilenameFromArgv() {
	const adocFilename = process.argv[2];
	assert(
		adocFilename,
		"You must specify an asciidoctor file to be previewed! Example:\nnode preview.js test\\test.adoc"
	);
	return normalizeSeparator(adocFilename);
}

/**
 * @param {string} adocFilename
 */
export function convertAdocFile(adocFilename) {
	processor.convertFile(adocFilename, {
		backend: "revealjs",
		safe: "unsafe",
	});
}

/**
 * @param {string} filename
 */
export function normalizeSeparator(filename) {
	return filename.replace(/\\/g, "/");
}
